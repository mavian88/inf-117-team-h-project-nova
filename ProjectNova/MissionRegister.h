#ifndef MISSIONREGISTER_H
#define MISSIONREGISTER_H

#include <boost/thread.hpp>
#include "Mission.h"

using namespace boost;
using namespace std;


//Singleton class
class MissionRegister
{
    public:
        static MissionRegister& getInstance()
        {
            static MissionRegister instance; // Guaranteed to be destroyed.
											 // Instantiated on first use.
            return instance;
        }
		static void sendToMissionRegister(Mission*);
		static vector<Mission*>* getMissions(int id);
		static mutex mtx;
    private:
		static vector<Mission*>* missionRegister;
        MissionRegister() {};
        MissionRegister(MissionRegister const&);    // Don't Implement
        void operator=(MissionRegister const&);		// Don't implement
};

#endif