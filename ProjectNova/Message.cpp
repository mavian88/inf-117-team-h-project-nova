#include "Message.h"

Message::Message(void)
{ }

Message::Message(int id, string name, Node* source, Node* destination, string type)
{
	this->id = id;
	this->name = name;
	this->source = source;
	this->destination = destination;
	this->type = type;
}

Message::~Message(void)
{
	delete source;
	delete destination;
}

void Message::setSource(Node* source)
{
	this->source = source;
}

void Message::setDestination(Node* destination)
{
	this->destination = destination;
}

int Message::getId()
{
	return id;
}

string Message::getName()
{
	return name;
}

Node* Message::getSource()
{
	return source;
}

Node* Message::getDestination()
{
	return destination;
}

string Message::getType()
{
	return type;
}

ostream& operator<<(std::ostream &strm, const Message &m) {
  return strm << m.name;
}