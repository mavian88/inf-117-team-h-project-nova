#include "MissionRegister.h"


vector<Mission*>* MissionRegister::missionRegister = new vector<Mission*>();
mutex MissionRegister::mtx;

void MissionRegister::sendToMissionRegister(Mission* m)
{
	mtx.lock();
	missionRegister->push_back(m);
	mtx.unlock();
}

vector<Mission*>* MissionRegister::getMissions(int id)
{
	mtx.lock();
	vector<Mission*>* missions (new vector<Mission*>());

	//collect all the missions addressed to the node that calls this method
	for(Mission* m : *missionRegister)
	{
		if(m->getDestination()->getId() == id)
		{
			missions->push_back(m);
		}
	}

	//remove the collected missions from the mission register
	for(Mission* m : *missions)
	{
		missionRegister->erase( remove(missionRegister->begin(), missionRegister->end(), m), missionRegister->end() );
	}
	mtx.unlock();

	//return the missions addressed to the calling node
	return missions;
}