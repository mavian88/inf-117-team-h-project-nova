#include "Mission.h"

Mission::Mission(void) : Message()
{ }

Mission::Mission(int id, string name, Node* source, Node* destination, Node* collection_target, int collection_duration)
	: Message(id, name, source, destination, "Mission")
{ 
	this->collection_target = collection_target;
	this->collection_duration = collection_duration;
}

Mission::~Mission(void)
{
	delete collection_target;
}

Node* Mission::getTarget(void)
{
	return collection_target;
}

int Mission::getDuration(void)
{
	return collection_duration;
}

bool Mission::operator==(const Mission& m) const
{
	return this->id == m.id;
}

ostream& operator<<(std::ostream &strm, const Mission &m) {
	return strm << m.name
				//<< " [Target: " << *(m.collection_target) << ", Duration: "<< m.collection_duration << "]";
				<< " [Target: " << *(m.collection_target) << "]";
}