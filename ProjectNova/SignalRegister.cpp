#include "SignalRegister.h"


vector<Signal*>* SignalRegister::signalRegister = new vector<Signal*>();
mutex SignalRegister::mtx;

void SignalRegister::sendToSignalRegister(Signal* s)
{
	mtx.lock();
	signalRegister->push_back(s);
	mtx.unlock();
}

vector<Signal*>* SignalRegister::getSignals(int id)
{
	mtx.lock();
	vector<Signal*>* signals (new vector<Signal*>());

	//collect all the signals addressed to the node that calls this method
	for(Signal* s : *signalRegister)
	{
		if(s->getDestination()->getId() == id)
		{
			signals->push_back(s);
		}
	}

	//remove the collected signals from the Signal register
	for(Signal* s : *signals)
	{
		signalRegister->erase( remove(signalRegister->begin(), signalRegister->end(), s), signalRegister->end() );
	}
	mtx.unlock();

	//return the signals addressed to the calling node
	return signals;
}