#include "NodeWithRoutingInfo.h"
#include "NodeMap.h"
#include "NodeWindowTable.h"
#include "Logger.h"

extern NodeWindowTable* nwt;

NodeWithRoutingInfo::NodeWithRoutingInfo() : Node()
{ }

NodeWithRoutingInfo::NodeWithRoutingInfo(int id, string name, string type) : Node(id, name, type)
{ }

NodeWithRoutingInfo::~NodeWithRoutingInfo(void)
{ }

int NodeWithRoutingInfo::getOptimalNextHop(int target_id, int m_id)
{
	//the routing information pertaining to the optimal next hop
	//  initialized with a routing distance of +infinity
	//  and a dummy routing predecessor 
	RoutingInfo* nextHopInfo = new RoutingInfo();

	//container for the relays that are currently reachable by this node
	vector<int>* reachableRELs = new vector<int>();

	for(int r : *getNodesWithLOSToMe()){
		//if I have LOS with the target, then the optimal next hop is the target itself
		if(r == target_id){
			return r;
		}

		//otherwise, keep track of this node's reachable relays
		if(NodeMap::at(r)->getType() == "Relay"){
			reachableRELs->push_back(r);
		}
	}

	//if this line is reached, it means I don't have LOS with the target,
	//  in this case, find the optimal next hop relay
	Node::LOS_mtx.lock();
	Logger::logEvent(getName() + " : Target[" + NodeMap::at(target_id)->getName() + "] \n", true, m_id);

	//for each reachable relay
	for(int r_id : *reachableRELs){
		//calculate its (relay's) routing distance to the target
		NodeWithRoutingInfo rel = *( (NodeWithRoutingInfo*) NodeMap::at(r_id) );
		unsigned int d = rel.calculateRoutingDistance(target_id, new vector<int>());

		Logger::logEvent("  Distance from " + rel.getName() + " -- " + to_string(d) + " \n", true, m_id);

		//update the optimal next hop's routing information accordingly
		//  the routing information will get updated if the relay's routing distance to the target
		//  is less than the current optimal routing distance
		nextHopInfo->update(d, rel.getId());
	}
	Node::LOS_mtx.unlock();

	delete reachableRELs;

	//if the target is completely unreachable as of right now (routing distance to it would be infinity)
	//  (this could happen if window.dat or any other window file used is missing some information or incompletely set up)
	//  set the optimal next hop to be this node itself
	if(nextHopInfo->getDistance() == UINT_MAX){
		nextHopInfo->update(UINT_MAX-1, getId());
	}

	//the optimal next hop is then returned
	return nextHopInfo->getPredecessor();
}

//recursive method 
//  calculates the number of hops required to reach a target, starting from this node
//  parameters: 
//		target_id : the id of the node that we are trying to reach
//		visitedRELs : the relays that have been visited so far (used to prevent non-deterministic recursion from happening)
//  returns: the number of hops that it would take to reach the node with id=target_id, starting from this node
int NodeWithRoutingInfo::calculateRoutingDistance(int target_id, vector<int>* visitedRELs)
{
	visitedRELs->push_back(getId());
	vector<int> reachables = *getNodesWithLOSToMe();

	for(int r : reachables){
		//Base case: if I have LOS with the target, then my routing distance to the target is 1
		if(r == target_id){
			return 1;
		}
	}

	//if I don't have LOS to the target, calculate the routing distance to the target with respect
	//  to the relays in which I currently have LOS with
	for(int r : reachables){
		if(NodeMap::at(r)->getType() == "Relay"
			&& find(visitedRELs->begin(), visitedRELs->end(), r) == visitedRELs->end()){

			NodeWithRoutingInfo rel = *( (NodeWithRoutingInfo*) NodeMap::at(r) );
			int distance = 1 + rel.calculateRoutingDistance(target_id, visitedRELs);

			return distance;
		}
	}

	//this code will be reached if the target happens to be unreachable
	//  i.e. this node has no way of reaching the target, no reachable relay can reach the target
	return UINT_MAX;
}

//method that returns a collection of IDs pertaining to those nodes 
//  in which this node currently has Line Of Sight (LOS) with
vector<int>* NodeWithRoutingInfo::getNodesWithLOSToMe()
{
	return nwt->getReachableNodes(this->getId());
}

//RoutingInfo Stuff
RoutingInfo::RoutingInfo() {
	distance = UINT_MAX;
	predecessor = -1;
}

RoutingInfo::~RoutingInfo() 
{ }

//try to update a node's distance and predecessor. return false if nothing was updated
bool RoutingInfo::update(unsigned int distance, int predecessor) {
	if (distance < this->distance) {
		this->distance = distance;
		this->predecessor = predecessor;
		return true;
	}
	return false;
}

unsigned int RoutingInfo::getDistance() {
	return distance;
}

int RoutingInfo::getPredecessor() {
	return predecessor;
}