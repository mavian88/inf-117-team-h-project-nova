#include "Node.h"
#include "Mission.h"


mutex Node::LOS_mtx;

Node::Node(void)
{ }

Node::Node(Node &n)
{ 
	this->id = n.getId();
	this->name = n.getName();
	this->type = n.getType();
}

Node::Node(int id, string name, string type)
{
	this->id = id;
	this->name = name;
	this->type = type;
}

Node::~Node(void)
{ }


int Node::getId()
{
	return id;
}

string Node::getName()
{
	return name;
}

string Node::getType()
{
	return type;
}

bool operator==(const Node& n1, const Node& n2) {
	return n1.id == n2.id;
}

ostream& operator<<(std::ostream &strm, const Node &n) {
  return strm << n.name;
}