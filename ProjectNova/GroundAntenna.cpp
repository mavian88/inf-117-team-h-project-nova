#include "GroundAntenna.h"
#include "Mission.h"
#include "MissionRegister.h"
#include "SignalRegister.h"
#include "Logger.h"
#include "NodeMap.h"

using namespace boost;


mutex GroundAntenna::ant_mtx;

GroundAntenna::GroundAntenna(void) : NodeWithRoutingInfo()
{ }

GroundAntenna::GroundAntenna(int id, string name) : NodeWithRoutingInfo(id, name, "Antenna")
{ }

GroundAntenna::~GroundAntenna(void)
{ }

void GroundAntenna::operator()()
{
	Logger::logEvent(getName() + " has started.. \n");

	while(true)
	{
		this_thread::sleep(posix_time::milliseconds(1));

		transmitMissions();
		transmitSignals();
	}
}

void GroundAntenna::transmitMissions()
{
	//get all missions addressed to me
	missionBuffer = MissionRegister::getMissions(getId());

	if(missionBuffer->empty()){
		return;
	}

	for(Mission* m : *missionBuffer)
	{
		//Let's say that transmission takes 1 second to complete
		this_thread::sleep(posix_time::milliseconds(1000));

		ant_mtx.lock();
		Logger::logEvent("[" + m->getName() + "] gets relayed to " + getName() + ". \n", true, m->getId());

		//WRITE ALGORITHM HERE TO DETERMINE TO WHICH NODE (COLLECTOR or RELAY)
		//  SHOULD A MISSION BE FORWARDED TO
		//Get the nodes that I can connect to
		vector<int>* reachables = getNodesWithLOSToMe();
		if(!reachables->empty()){
			Node::LOS_mtx.lock();
			Logger::logEvent(getName() + " has LOS to [", true, m->getId());
			for(unsigned int i=0; i<reachables->size(); i++){
				NodeWithRoutingInfo* n = (NodeWithRoutingInfo*) NodeMap::at(reachables->at(i));
				Logger::logEvent(n->getName() + (i==reachables->size()-1 ? "" : ", "), false, m->getId() );
			}
			Logger::logEvent("]\n", false, m->getId());
			Node::LOS_mtx.unlock();
		}

		//determine the most optimal next hop for this mission
		Node* destination = NodeMap::at( getOptimalNextHop(m->getTarget()->getId(), m->getId()) );

		Node::LOS_mtx.lock();
		//if the optimal next hop happens to the the target (i.e. I have LOS to the target Collector)
		if(destination->getId() == m->getTarget()->getId()){
			Logger::logEvent(getName() + " has LOS to target " + m->getTarget()->getName() + ".. ", true, m->getId() );	
		}
		//otherwise
		else{
			Logger::logEvent(getName() + " has no LOS to target " + m->getTarget()->getName() + ".. ", true, m->getId() );			
		}

		delete reachables;

		//forward the mission to the chosen destination (target collector or relay),
		//  change the source and destination attributes of this mission
		Node* source = this;
		m->setSource(source);
		m->setDestination(destination);

		Logger::logEvent(getName() + " forwards [" + m->getName() + "] to " + to_string(*destination) + ". \n", false, m->getId());
		Node::LOS_mtx.unlock();
		ant_mtx.unlock();

		MissionRegister::sendToMissionRegister(m);
	}
}

void GroundAntenna::transmitSignals()
{
	//Check if there's a signal to that is addressed to me
	signalBuffer = SignalRegister::getSignals(getId());

	if(signalBuffer->empty()){
		return;
	}

	for(Signal* s : *signalBuffer)
	{
		ant_mtx.lock();
		Logger::logEvent(getName() + " receives [" + s->getName() + "]. \n", true, s->getId());
		ant_mtx.unlock();
	}

	//FUTURE FEATURE: WRITE ALGORITHM HERE TO DETERMINE TO WHICH NODE 
	//  (Mission Processing) SHOULD A SIGNAL BE FORWARDED TO
}