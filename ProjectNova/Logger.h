#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

using namespace boost;
using namespace std;


//Singleton class
class Logger
{
    public:
        static Logger& getInstance()
        {
            static Logger instance; // Guaranteed to be destroyed.
									// Instantiated on first use.
            return instance;
        }
		static void setOutFile(string, int ms = 0);
		static void closeOutFile();
		static void logEvent(string evt, bool isTimeStamped = true, int m = -1);
		static string simlog_path;
		static mutex log;
    private:
        Logger() {};
        Logger(Logger const&);			 // Don't Implement
        void operator=(Logger const&);	// Don't implement
		static ofstream outFile;
		static vector<ofstream> mFile;
};

#endif