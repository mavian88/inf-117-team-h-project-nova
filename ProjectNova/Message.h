#ifndef MESSAGE_H
#define MESSAGE_H

#include "Node.h"

class Message
{
protected:
	int id;
	string name;
	Node* source;
	Node* destination;
	string type;
	friend std::ostream& operator<<(std::ostream&, const Message&);

public:
	Message(void);
	Message(int, string, Node*, Node*, string);
	~Message(void);
	void setSource(Node*);
	void setDestination(Node*);
	int getId();
	Node* getSource();
	Node* getDestination();
	string getName();
	string getType();
};

#endif