#ifndef SIGNALRELAY_H
#define SIGNALRELAY_H

#include "NodeWithRoutingInfo.h"

class SignalRelay : public NodeWithRoutingInfo
{
private:
	void transmitMissions();
	void transmitSignals();
public:
	SignalRelay(void);
	SignalRelay(int id, string name);
	~SignalRelay(void);
	void operator()();

	static mutex rel_mtx;
};

#endif