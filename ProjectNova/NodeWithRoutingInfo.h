#ifndef NODEWITHROUTINGINFO_H
#define NODEWITHROUTINGINFO_H

#include "Node.h"


class RoutingInfo
{
private:
	unsigned int distance;
	int predecessor;	

public:
	RoutingInfo();
	~RoutingInfo();
	bool update(unsigned int distance, int predecessor);
	unsigned int getDistance();
	int getPredecessor();
};


class NodeWithRoutingInfo : public Node
{
public:
	NodeWithRoutingInfo(void);
	NodeWithRoutingInfo(int, string, string);
	~NodeWithRoutingInfo(void);
	int getOptimalNextHop(int, int);
	int calculateRoutingDistance(int, vector<int>*);
	vector<int>* getNodesWithLOSToMe();
};

#endif