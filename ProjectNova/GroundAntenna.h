#ifndef GROUNDANTENNA_H
#define GROUNDANTENNA_H

#include "NodeWithRoutingInfo.h"
#include "Mission.h"
#include "Signal.h"

class GroundAntenna : public NodeWithRoutingInfo
{
private:
	void transmitMissions();
	void transmitSignals();

public:
	GroundAntenna(void);
	GroundAntenna(int id, string name);
	~GroundAntenna(void);
	void operator()();

	static mutex ant_mtx;
};

#endif