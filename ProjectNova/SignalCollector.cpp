#include "SignalCollector.h"
#include "Mission.h"
#include "Signal.h"
#include "MissionRegister.h"
#include "SignalRegister.h"
#include "NodeMap.h"
#include "Logger.h"

using namespace boost;


mutex SignalCollector::col_mtx;

SignalCollector::SignalCollector(void)
{ }

SignalCollector::SignalCollector(int id, string name) : NodeWithRoutingInfo(id, name, "Collector")
{ }

SignalCollector::~SignalCollector(void)
{ }

void SignalCollector::operator()()
{
	Logger::logEvent(getName() + " has started.. \n");

	while(true)
	{
		this_thread::sleep(posix_time::milliseconds(1));

		transmitMissions();
		transmitSignals();
	}
}

void SignalCollector::transmitMissions()
{
	//get all missions addressed to me
	missionBuffer = MissionRegister::getMissions(getId());

	//Check if there's a mission to be processed
	if( missionBuffer->empty() ){
		return;
	}

	for(Mission* m : *missionBuffer)
	{
		//Let's say that transmission takes 1 second to complete
		this_thread::sleep(posix_time::seconds(1));

		col_mtx.lock();
		Logger::logEvent(getName() + " receives [" + m->getName() + "]. \n", true, m->getId());

		//Let's say that it takes 1 second to prepare the signal
		this_thread::sleep(posix_time::seconds(1));
		Signal* s (new Signal(m->getId(), 
								"SGN " + to_string(m->getId()), 
								this, 
								this, 
								m->getSource(), 
								m->getDuration()));
		Logger::logEvent(to_string(*s) + " has been created. \n", true, m->getId());
		SignalRegister::sendToSignalRegister(s);
		col_mtx.unlock();
	}
}

void SignalCollector::transmitSignals()
{
	//Check if there's a signal to be sent
	signalBuffer = SignalRegister::getSignals(getId());

	if(signalBuffer->empty()){
		return;
	}

	//WRITE ALGORITHM HERE TO DETERMINE TO WHICH NODE  (RELAY or GROUND ANTENNA)
	//  SHOULD A SIGNAL BE FORWARDED TO
	for(Signal* s : *signalBuffer)
	{
		for(int i=0; i<s->getDuration(); i+=1000){
			col_mtx.lock();

			//Get the nodes that I can connect to
			vector<int>* reachables = getNodesWithLOSToMe();
			if(!reachables->empty()){
				Node::LOS_mtx.lock();
				Logger::logEvent(getName() + " has LOS to [", true, s->getId());
				for(unsigned int i=0; i<reachables->size(); i++){
					NodeWithRoutingInfo* n = (NodeWithRoutingInfo*) NodeMap::at(reachables->at(i));
					Logger::logEvent(n->getName() + (i==reachables->size()-1 ? "" : ", "), false, s->getId() );
				}
				Logger::logEvent("]\n", false, s->getId());
				Node::LOS_mtx.unlock();
			}

			//determine the most optimal next hop for this signal
			Node* destination = NodeMap::at( getOptimalNextHop(s->getTarget()->getId(), s->getId()) );

			Node::LOS_mtx.lock();
			//if the optimal next hop happens to the the target (i.e. I have LOS to the target Antenna)
			if(*destination == *(s->getTarget())){
				Logger::logEvent(getName() + " has LOS to target " + s->getTarget()->getName() + ".. ", true, s->getId() );	
			}
			//otherwise
			else{
				Logger::logEvent(getName() + " has no LOS to target " + s->getTarget()->getName() + ".. ", true, s->getId() );			
			}

			delete reachables;

			//forward the signal to the chosen destination (target antenna or relay),
			//  change the source and destination attributes of this mission
			//Node* source = this;
			//s->setSource(source);
			s->setDestination(destination);

			Logger::logEvent(getName() + " forwards [" + s->getName() + "] to " + to_string(*destination) + ". \n", false, s->getId());
			Node::LOS_mtx.unlock();
			col_mtx.unlock();

			//Let's say signal transmission takes 1 second
			this_thread::sleep(posix_time::seconds(1));

			SignalRegister::sendToSignalRegister(s);
		}
	}
}