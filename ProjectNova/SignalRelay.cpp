#include "SignalRelay.h"
#include "Mission.h"
#include "MissionRegister.h"
#include "SignalRegister.h"
#include "NodeMap.h"
#include "Logger.h"

using namespace boost;


mutex SignalRelay::rel_mtx;

SignalRelay::SignalRelay(void)
{ }

SignalRelay::SignalRelay(int id, string name) : NodeWithRoutingInfo(id, name, "Relay")
{ }

SignalRelay::~SignalRelay(void)
{ }

void SignalRelay::operator()()
{
	Logger::logEvent(getName() + " has started.. \n");

	while(true)
	{
		this_thread::sleep(posix_time::milliseconds(1));

		transmitMissions();
		transmitSignals();
	}
}

void SignalRelay::transmitMissions()
{
	//get all missions addressed to me
	missionBuffer = MissionRegister::getMissions(getId());

	if(missionBuffer->empty()){
		return;
	}

	for(Mission* m : *missionBuffer)
	{
		//Let's say that transmission takes 1 second to complete
		this_thread::sleep(posix_time::milliseconds(1000));

		rel_mtx.lock();
		Logger::logEvent("[" + m->getName() + "] gets relayed to " + getName() + ". \n", true, m->getId());

		//WRITE ALGORITHM HERE TO DETERMINE TO WHICH NODE (COLLECTOR or RELAY)
		//  SHOULD A MISSION BE FORWARDED TO
		//Get the nodes that I can connect to
		vector<int>* reachables = getNodesWithLOSToMe();
		if(!reachables->empty()){
			Node::LOS_mtx.lock();
			Logger::logEvent(getName() + " has LOS to [", true, m->getId());
			for(unsigned int i=0; i<reachables->size(); i++){
				NodeWithRoutingInfo* n = (NodeWithRoutingInfo*) NodeMap::at(reachables->at(i));
				Logger::logEvent(n->getName() + (i==reachables->size()-1 ? "" : ", "), false, m->getId());
			}
			Logger::logEvent("]\n", false, m->getId());
			Node::LOS_mtx.unlock();
		}

		//determine the most optimal next hop for this mission
		Node* destination = NodeMap::at( getOptimalNextHop(m->getTarget()->getId(), m->getId()) );

		Node::LOS_mtx.lock();
		//if the optimal next hop happens to the the target (i.e. I have LOS to the target Collector)
		if(destination->getId() == m->getTarget()->getId()){
			Logger::logEvent(getName() + " has LOS to target " + m->getTarget()->getName() + ".. ", true, m->getId() );	
		}
		//otherwise
		else{
			Logger::logEvent(getName() + " has no LOS to target " + m->getTarget()->getName() + ".. ", true, m->getId() );			
		}

		delete reachables;

		//WHEN A RELAY FORWARDS A MISSION THAT WAS ADDRESSED TO THEM, DON'T CHANGE THE MISSION's SOURCE PROPERTY
		//  KEEP THE SOURCE INFO AS IT IS (pointer to the Antenna that first forwarded the mission to the Relay)
		//  THE FINAL RECIPIENT OF OF THE MISSION (Target Collector) WILL USE THE SOURCE INFO
		//  TO DETERMINE WHICH GROUND ANTENNA SHOULD THE SIGNAL BE TRANSMITTED TO

		//forward the mission to the chosen destination (target collector or relay),
		//  change ONLY th destination attribute of this mission
		m->setDestination(destination);

		Logger::logEvent(getName() + " forwards [" + m->getName() + "] to " + to_string(*destination) + ". \n", false, m->getId());
		Node::LOS_mtx.unlock();
		rel_mtx.unlock();

		MissionRegister::sendToMissionRegister(m);
	}
}

void SignalRelay::transmitSignals()
{
	//Check if there's a signal to be sent
	signalBuffer = SignalRegister::getSignals(getId());

	if(signalBuffer->empty()){
		return;
	}

	//WRITE ALGORITHM HERE TO DETERMINE TO WHICH NODE  (RELAY or GROUND ANTENNA)
	//  SHOULD A SIGNAL BE FORWARDED TO
	for(Signal* s : *signalBuffer)
	{
		rel_mtx.lock();
		Logger::logEvent("[" + s->getName() + "] gets relayed to " + getName() + ". \n", true, s->getId());

		//Get the nodes that I can connect to
		vector<int>* reachables = getNodesWithLOSToMe();
		if(!reachables->empty()){
			Node::LOS_mtx.lock();
			Logger::logEvent(getName() + " has LOS to [", true, s->getId());
			for(unsigned int i=0; i<reachables->size(); i++){
				NodeWithRoutingInfo* n = (NodeWithRoutingInfo*) NodeMap::at(reachables->at(i));
				Logger::logEvent(n->getName() + (i==reachables->size()-1 ? "" : ", "), false, s->getId() );
			}
			Logger::logEvent("]\n", false, s->getId());
			Node::LOS_mtx.unlock();
		}

		//determine the most optimal next hop for this signal
		Node* destination = NodeMap::at( getOptimalNextHop(s->getTarget()->getId(), s->getId()) );

		Node::LOS_mtx.lock();
		//if the optimal next hop happens to the the target (i.e. I have LOS to the target Antenna)
		if(*destination == *(s->getTarget())){
			Logger::logEvent(getName() + " has LOS to target " + s->getTarget()->getName() + ".. ", true, s->getId() );	
		}
		//otherwise
		else{
			Logger::logEvent(getName() + " has no LOS to target " + s->getTarget()->getName() + ".. ", true, s->getId() );			
		}

		delete reachables;

		//forward the signal to the chosen destination (target antenna or relay),
		//WHEN A RELAY FORWARDS A SIGNAL THAT WAS ADDRESSED TO THEM, DON'T CHANGE THE SIGNAL's SOURCE PROPERTY
		//  KEEP THE SOURCE INFO AS IT IS (pointer to the Collector that first forwarded the signal to the Relay)
		//  THE FINAL RECIPIENT OF OF THE SIGNAL (Target Antenna) WILL USE THE SOURCE INFO
		//  TO DETERMINE WHICH COLLECTOR SHOULD THE REPEATED MISSION BE TRANSMITTED TO
		s->setDestination(destination);

		Logger::logEvent(getName() + " forwards [" + s->getName() + "] to " + to_string(*destination) + ". \n", false, s->getId());
		Node::LOS_mtx.unlock();
		rel_mtx.unlock();

		//Let's say signal transmission takes 1 second
		this_thread::sleep(posix_time::milliseconds(1000));

		SignalRegister::sendToSignalRegister(s);
	}
}
