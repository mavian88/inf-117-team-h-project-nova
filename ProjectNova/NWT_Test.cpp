//#include "Node.h"
//#include "MissionPlanning.h"
//#include "CommandCenter.h"
//#include "GroundAntenna.h"
//#include "SignalRelay.h"
//#include "SignalCollector.h"
//#include "NodeMap.h"
//#include "NodeWindowTable.h"
//#include "Logger.h"
//
//using namespace boost;
//using namespace std;
//
//NodeWindowTable* nwt;
//
////global variables for the simulation
//const int MPL_COUNT = 1;	//Mission Planning
//const int COM_COUNT = 3;	//Command and Control (Command Center)
//const int ANT_COUNT = 6;	//Gound Antenna
//const int REL_COUNT = 4;	//Signal Relay
//const int COL_COUNT = 12;	//Signal Collector
//const int MPR_COUNT = 3;	//Mission Processing
//const int ANA_COUNT = 3;	//Analysts
//const int USR_COUNT = 100;	//Users
//
//MissionPlanning* planning;
//CommandCenter* commCenters[COM_COUNT];
//GroundAntenna* antennas[ANT_COUNT];
//SignalRelay* relays[REL_COUNT];
//SignalCollector* collectors[COL_COUNT];
//
//int main(void){
//	int id = 0;
//	//load mission planning to the simulator
//	//Mission Planning's ID range: 0
//	//planning = new MissionPlanning(id, "MPL", mission_file);
//	//cout<<*planning<<" has been created.. "<<endl;
//	//NodeMap::insert(id, planning);
//
//	//load command centers to the simulator
//	//Command Center's ID range: 1-3
//	cout<<endl;
//	for(int i=0; i<COM_COUNT; i++){
//		commCenters[i] = new CommandCenter(++id, "COM " + to_string(i+1));
//		cout<<*commCenters[i]<<" has been created.. "<<endl;
//		NodeMap::insert(id, commCenters[i]);
//	}
//
//	//load ground antennas to the simulator
//	//Ground Antenna's ID range: 4-9
//	cout<<endl;
//	for(int i=0; i<ANT_COUNT; i++){
//		antennas[i] = new GroundAntenna(++id, "ANT " + to_string(i+1));
//		cout<<*antennas[i]<<" has been created.. "<<endl;
//		NodeMap::insert(id, antennas[i]);
//	}
//
//	//load relays to the simulator
//	//Signal Relay's ID range: 10-13
//	cout<<endl;
//	for(int i=0; i<REL_COUNT; i++){
//		relays[i] = new SignalRelay(++id, "REL " + to_string(i+1));
//		cout<<*relays[i]<<" has been created.. "<<endl;
//		NodeMap::insert(id, relays[i]);
//	}
//
//	//load collectors to the simulator
//	//Signal Collector's ID range: 14-25
//	cout<<endl;
//	for(int i=0; i<COL_COUNT; i++){
//		collectors[i] = new SignalCollector(++id, "COL " + to_string(i+1));
//		cout<<*collectors[i]<<" has been created.. "<<endl;
//		NodeMap::insert(id, collectors[i]);
//	}
//
//
//	int from = 15;
//	nwt = new NodeWindowTable("window2.dat");
//	thread nwt_thread(*nwt);
//	while(true){
//		
//		cout<< *NodeMap::at(from) << " has LOS to [ ";
//		for(int n : *(nwt->getReachableNodes(from))){
//			cout<< *NodeMap::at(n) << " ";
//		}
//		cout<<"] \n";
//
//		this_thread::sleep(posix_time::seconds(5));
//	}
//}