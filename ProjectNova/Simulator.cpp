#pragma once

#include <cstdlib>
#include<Windows.h>
#include <boost/thread.hpp>

#include "Node.h"
#include "MissionPlanning.h"
#include "CommandCenter.h"
#include "GroundAntenna.h"
#include "SignalRelay.h"
#include "SignalCollector.h"
#include "NodeMap.h"
#include "NodeWindowTable.h"
#include "Logger.h"

using namespace boost;
using namespace std;

NodeWindowTable* nwt;

//global variables for the simulation
const int MPL_COUNT = 1;	//Mission Planning
const int COM_COUNT = 3;	//Command and Control (Command Center)
const int ANT_COUNT = 6;	//Gound Antenna
const int REL_COUNT = 4;	//Signal Relay
const int COL_COUNT = 12;	//Signal Collector
const int MPR_COUNT = 3;	//Mission Processing
const int ANA_COUNT = 3;	//Analysts
const int USR_COUNT = 100;	//Users

//name of the data_file that will be used by Mission Planning
string mission_file = "mission20.dat";

//name of the data_file that will be used by the Node Window Table
string window_file = "window.dat";

Node* dummy;
MissionPlanning* planning;
CommandCenter* commCenters[COM_COUNT];
GroundAntenna* antennas[ANT_COUNT];
SignalRelay* relays[REL_COUNT];
SignalCollector* collectors[COL_COUNT];


void printHeading()
{
	cout<<"\n"
		<<" ======================================================== \n"
		<<" = Space and Ground Asset Simulator for Aerospace Corp. = \n"
		<<" ======================================================== \n";
}

void printWelcomeMessage()
{
	printHeading();
	
	cout<<"\n\n";
	cout<<" Welcome! \n"
		<<" This application is a supplementary software for a system \n"
		<<" that simulates the interaction among orbiting satellites, \n"
		<<" space relays, and their ground assets. This application is \n"
		<<" responsible for simulating the dynamic routing of discrete \n"
		<<" missions and continuous signals between communicating nodes. \n";
	
	cout<<"\n   To start the application, press ENTER. ";
	getchar();
}

int prepareMissionFiles(string mission_file){
	typedef tokenizer<char_separator<char>> tokenizer;

	char_separator<char> sep("mission");
	tokenizer tok(mission_file, sep);

	for(tokenizer::iterator t=tok.begin(); t!=tok.end();++t){
		int mSize = atoi((*t).c_str());
		
		return mSize;
	}

	return 0;
}

void loadNodes()
{
	std::system("CLS");
	printHeading();

	//set up the logger
	time_t t = time(0);

	int mSize = prepareMissionFiles(mission_file);

	Logger::setOutFile("sim_log" + to_string(t) + ".txt", mSize);

	Logger::logEvent("\n Window file used:\t" + window_file, false);
	Logger::logEvent("\n Mission file used:\t" + mission_file + "\n\n", false);

	cout<<endl<<"Preparing simulation objects.. \n\n";
	this_thread::sleep(posix_time::milliseconds(2000));

	int id=0;
	
	//load mission planning to the simulator
	//Mission Planning's ID range: 0
	planning = new MissionPlanning(id, "MPL", mission_file);
	cout<<*planning<<" has been created.. "<<endl;
	NodeMap::insert(id, planning);

	//load command centers to the simulator
	//Command Center's ID range: 1-3
	cout<<endl;
	for(int i=0; i<COM_COUNT; i++){
		commCenters[i] = new CommandCenter(++id, "COM " + to_string(i+1));
		cout<<*commCenters[i]<<" has been created.. "<<endl;
		NodeMap::insert(id, commCenters[i]);
	}

	//load ground antennas to the simulator
	//Ground Antenna's ID range: 4-9
	cout<<endl;
	for(int i=0; i<ANT_COUNT; i++){
		antennas[i] = new GroundAntenna(++id, "ANT " + to_string(i+1));
		cout<<*antennas[i]<<" has been created.. "<<endl;
		NodeMap::insert(id, antennas[i]);
	}

	//load relays to the simulator
	//Signal Relay's ID range: 10-13
	cout<<endl;
	for(int i=0; i<REL_COUNT; i++){
		relays[i] = new SignalRelay(++id, "REL " + to_string(i+1));
		cout<<*relays[i]<<" has been created.. "<<endl;
		NodeMap::insert(id, relays[i]);
	}

	//load collectors to the simulator
	//Signal Collector's ID range: 14-25
	cout<<endl;
	for(int i=0; i<COL_COUNT; i++){
		collectors[i] = new SignalCollector(++id, "COL " + to_string(i+1));
		cout<<*collectors[i]<<" has been created.. "<<endl;
		NodeMap::insert(id, collectors[i]);
	}

	cout<<endl<<"Output file 'sim_log" + to_string(t) + ".txt' has been created. \n";

	cout<<endl<<"Simulation initialization completed. \n\n"
	<<"Press ENTER to start the simulation. ";

	//"start" the simulation after the user presses enter
	getchar();
}

void startNodes()
{
	std::system("CLS");
	printHeading();
	cout<<"\n"
		<<"[TO TERMINATE THE SIMULATION AT ANY TIME, PRESS ENTER] \n\n"
		<<" Simulation starting in ";
	COORD coord;
	coord.X = 25;
	coord.Y = 7;
	for(int countdown = 5; countdown > 0; countdown--){
		SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ), coord);
		
		cout<<countdown<<".. ";
		this_thread::sleep(posix_time::milliseconds(1000));
	}
	cout<<endl;

	//start the dynamic Node Window Table
	cout<<endl;
	nwt = new NodeWindowTable(window_file);
	//nwt = new NodeWindowTable(window_file, true);
	thread nwt_thread(*nwt);

	//start Mission Planning
	cout<<endl;
	thread mpl_thread(*planning);

	//start the command centers
	cout<<endl;
	thread com_threads[COM_COUNT];
	for(int i=0; i<COM_COUNT; i++){
		com_threads[i] = thread(*commCenters[i]);
	}

	//start the antennas
	cout<<endl;
	thread ant_threads[ANT_COUNT];
	for(int i=0; i<ANT_COUNT; i++){
		ant_threads[i] = thread(*antennas[i]);
	}

	//start the relays
	cout<<endl;
	thread rel_threads[REL_COUNT];
	for(int i=0; i<REL_COUNT; i++){
		rel_threads[i] = thread(*relays[i]);
	}

	//start the collectors
	cout<<endl;
	thread col_threads[COL_COUNT];
	for(int i=0; i<COL_COUNT; i++){
		col_threads[i] = thread(*collectors[i]);
	}

	//terminate the simulation after the user presses enter
	getchar();

	//'terminate' all the simulator threads
	//NodeWindowTable
	nwt_thread.interrupt();
	//MissionPlanning
	mpl_thread.interrupt();
	//CommandCenter
	for(int i=0; i<COM_COUNT; i++){
		com_threads[i].interrupt();
	}
	//GroundAntenna
	for(int i=0; i<ANT_COUNT; i++){
		ant_threads[i].interrupt();
	}
	//SignalRelay
	for(int i=0; i<REL_COUNT; i++){
		rel_threads[i].interrupt();
	}
	//SignalCollector
	for(int i=0; i<COL_COUNT; i++){
		col_threads[i].interrupt();
	}
}

void exitRoutine()
{
	std::system("CLS");
	printHeading();
	cout<<"\n\n"
		<<" SIMULATION TERMINATED. \n"
		<<" Simulation log file is stored in " + Logger::simlog_path<<"\n";

	//close the log output file at the end of the simulation
	Logger::closeOutFile();

	cout<<"\n   PRESS [ENTER] TO CLOSE THE APPLICATION. \n";
	getchar();
}

void getFileNames()
{
	cout << endl << " Enter the window table file:\t";
	cin >> window_file;
	cout << " Enter the mission file:\t";
	cin >> mission_file;

	getchar();
}

int main(void)
{
	//print introductory message to the user, explain what the application does, 
	//  and provide some instructions on how to use the simulation
	printWelcomeMessage();

	// prompt user for desired files
	getFileNames();

	//load the necessary nodes for the simulator
	loadNodes();

	//start the thread for each nodes
	startNodes();

	exitRoutine();

	return 0;
}