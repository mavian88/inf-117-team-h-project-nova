#include <fstream>
#include "MissionPlanning.h"
#include "Mission.h"
#include "CommandCenter.h"
#include "SignalCollector.h"
#include "MissionRegister.h"
#include "Logger.h"
#include "NodeMap.h"

using namespace boost;


bool MissionPlanning::done = false;

MissionPlanning::MissionPlanning(void) : Node()
{ }

MissionPlanning::MissionPlanning(int id, string name, string mission_file) : Node(id, name, "Mission Planning")
{
	this->mission_file = mission_file;
}

MissionPlanning::~MissionPlanning(void)
{ }

void MissionPlanning::operator()()
{
	this_thread::sleep(posix_time::milliseconds(1000));
	ifstream inFile;
	inFile.open("missions/" + mission_file);

	if( inFile.fail() )
	{
		Logger::logEvent("\n ** Error opening mission planning file. \n");
		exit(1);
	}

	Logger::logEvent(getName() + " has started.. \n");

	//mission attributes
	int mission_id, 
		target_id;
	int start_time;
	int duration;

	//used to calculate the amount of time that Mission Planning should wait in between the sending of missions
	int prev_time = 0;

	//the id of the command center in which the mission is to be sent to
	int next_comm = 1;

	while( !inFile.eof() )
	{
		//prepare the mission info
		inFile>>mission_id>>target_id>>start_time>>duration;

		//here mission planning waits for the arrival of the next mission
		int wait_time = start_time - prev_time;
		this_thread::sleep(posix_time::milliseconds(wait_time));

		//save the previous start_time to calculate the next wait_time
		prev_time = start_time;

		//start processing the mission info
		Node* c = NodeMap::at(next_comm);
		Node* t = NodeMap::at(target_id);

		//create a mission instance
		//Mission m = Mission(mission_id, 
		//						"MSN" + to_string(mission_id), i.e. "MSN 1"
		//						source node (Mission Planning), 
		//						destination node (one of the 3 Command and Control instances), 
		//						node that corresponds to target_id, 
		//						duration);	
		//send the new mission to a Command Center (COM),
		//  use round robin to decide which COM should the mission be sent to
		Mission* m ( new Mission(mission_id, "MSN " + to_string(mission_id), this, c, t, duration) );
		Logger::logEvent("New Mission arrived.. " + to_string(*m) + " has been created. \n", true, mission_id);

		//get the id of the next Command Center to which the next mission will be sent to
		if(next_comm != 3) {
			next_comm = (++next_comm) % 4;
		}
		else {
			next_comm = 1;
		}

		Logger::logEvent(getName() + " sends [" + m->getName() + "] to " + to_string(*(m->getDestination())) + ".\n", true, mission_id);
		//register this new mission to let the other nodes know of this new mission
		MissionRegister::sendToMissionRegister(m);
	}

	Logger::logEvent("\n", false);
	Logger::logEvent(getName() + " is done loading missions. \n\n");
	done = true;
	inFile.close();
}

bool MissionPlanning::isDone()
{
	return done;
}