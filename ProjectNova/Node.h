
#ifndef NODE_H
#define NODE_H

#include <boost/thread.hpp>

using namespace std;
using namespace boost;

//use forward declaration to get rid of the circular reference problem
class Mission;
class Signal;

class Node
{
private:
	int id;
	string name;
	string type;
protected:
	vector<Mission*>* missionBuffer;
	vector<Signal*>* signalBuffer;
	friend bool operator==(const Node& n1, const Node& n2);
	friend std::ostream& operator<<(std::ostream&, const Node&);

public:
	Node(void);
	Node(Node&);
	Node(int, string, string);
	~Node(void);
	int getId();
	string getName();
	string getType();

	//added this to fix the logging issue where LOS logs of different nodes get mixed together
	static mutex LOS_mtx;
};

#endif