#ifndef MISSION_H
#define MISSION_H

#include "Message.h"

class Mission : public Message
{
private:
	Node* collection_target; //the target collector that will collect the signal
	int collection_duration; //for how long should the collector collect signals from the target
	friend std::ostream& operator<<(std::ostream&, const Mission&);

public:
	Mission(void);
	Mission(int, string, Node*, Node*, Node*, int);
	~Mission(void);
	Node* getTarget(void);
	int getDuration(void);
	bool operator==(const Mission&) const;
};

#endif