#ifndef NODEMAP_H
#define NODEMAP_H

#include "Node.h"
#include <boost/unordered_map.hpp>

using namespace std;


//Singleton class
class NodeMap
{
    public:
        static NodeMap& getInstance()
        {
            static NodeMap instance; // Guaranteed to be destroyed.
									 // Instantiated on first use.
            return instance;
        }

		static Node* at(int);
		static unordered_map<int, Node*>::iterator find(int);
		static void insert(int, Node*);
		static const unordered_map<int, Node*>* getMap();
		static const vector<int>* getKeys();
		static const vector<Node*>* getValues();

    private:
		static unordered_map<int, Node*>* nodeMap;
        NodeMap() {};
        NodeMap(NodeMap const&);    // Don't Implement
        void operator=(NodeMap const&);		// Don't implement
};

#endif