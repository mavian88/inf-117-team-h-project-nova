#ifndef SIGNALCOLLECTOR_H
#define SIGNALCOLLECTOR_H

#include "NodeWithRoutingInfo.h"


class SignalCollector : public NodeWithRoutingInfo
{
private: 
	void transmitMissions();
	void transmitSignals();

public:
	SignalCollector(void);
	SignalCollector(int id, string name);
	~SignalCollector(void);
	void operator()();

	static mutex col_mtx;
};

#endif