#include "Signal.h"


Signal::Signal(void) : Message()
{ }

Signal::Signal(int id, string name, Node* source, Node* destination, Node* signal_target, int signal_duration) 
	: Message(id, name, source, destination, "Signal")
{ 
	this->signal_target = signal_target;
	this->signal_duration = signal_duration;
}

Signal::Signal(const Signal& s) : Message(s.id, s.name, s.source, s.destination, "Signal")
{
	this->signal_target = s.signal_target;
	this->signal_duration = s.signal_duration;
}

Signal::~Signal(void)
{ 
	delete signal_target;
}

Node* Signal::getTarget()
{
	return signal_target;
}

int Signal::getDuration()
{
	return signal_duration;
}

bool Signal::operator==(const Signal& s) const
{
	return this->id == s.id;
}

ostream& operator<<(std::ostream &strm, const Signal &s) {
	return strm << s.name
				//<< " [Target: " << *(s.signal_target) << "]";
				<< " [Target: " << *(s.signal_target) << ", Duration: "<< s.signal_duration << "]";

}
