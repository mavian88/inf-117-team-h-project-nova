#ifndef SIGNALREGISTER_H
#define SIGNALREGISTER_H

#include <boost/thread.hpp>
#include "Signal.h"

using namespace boost;
using namespace std;


//Singleton class
class SignalRegister
{
    public:
        static SignalRegister& getInstance()
        {
            static SignalRegister instance; // Guaranteed to be destroyed.
											 // Instantiated on first use.
            return instance;
        }
		static void sendToSignalRegister(Signal*);
		static vector<Signal*>* getSignals(int id);
		static mutex mtx;
    private:
		static vector<Signal*>* signalRegister;
        SignalRegister() {};
        SignalRegister(SignalRegister const&);    // Don't Implement
        void operator=(SignalRegister const&);		// Don't implement
};


#endif