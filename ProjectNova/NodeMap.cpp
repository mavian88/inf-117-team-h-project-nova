#include "NodeMap.h"

unordered_map<int, Node*>* NodeMap::nodeMap = new unordered_map<int, Node*>();

Node* NodeMap::at(int id)
{
	return nodeMap->at(id);
}

unordered_map<int, Node*>::iterator NodeMap::find(int id)
{
	return nodeMap->find(id);
}

void NodeMap::insert(int id, Node* n)
{
	nodeMap->insert( make_pair(id, n) );
}

const unordered_map<int, Node*>* NodeMap::getMap()
{
	return nodeMap;
}

const vector<int>* NodeMap::getKeys()
{
	vector<int>* keys = new vector<int>();
	keys->reserve(nodeMap->size());

	for(auto k : *nodeMap) {
		keys->push_back(k.first);
	}

	return keys;
}

const vector<Node*>* NodeMap::getValues()
{
	vector<Node*>* values = new vector<Node*>();

	values->reserve(nodeMap->size());

	for(auto v : *nodeMap) {
		values->push_back(v.second);
	}

	return values;
}