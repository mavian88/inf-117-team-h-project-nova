#ifndef COMMANDCENTER_H
#define COMMANDCENTER_H

#include "node.h"


class CommandCenter : public Node
{
public:
	CommandCenter(void);
	CommandCenter(int, string); 
	~CommandCenter(void);
	void operator()();
};

#endif