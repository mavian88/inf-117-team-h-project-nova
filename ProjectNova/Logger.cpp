#include "Logger.h"

#include <direct.h>


using namespace boost::posix_time;
using namespace std;


ofstream Logger::outFile;
vector<ofstream> Logger::mFile;
string Logger::simlog_path;
mutex Logger::log;

void Logger::setOutFile(string out_file_name, int ms)
{
	simlog_path = "outputs/" + out_file_name;
	outFile.open( simlog_path );

	if( outFile.fail() )
	{
		Logger::logEvent("\n " + simlog_path + "\n ** Error opening output file. \n");
		exit(3);
	}

	mFile = vector<ofstream>(ms);

	if(ms > 0){
		time_t t = time(0);
		string mFolder = "trace" + to_string(t) + "/";
		_mkdir( ("outputs/mission_traces/" + mFolder).c_str() );

		for(int i=0; i<ms; i++){
			mFile.at(i).open("outputs/mission_traces/" + mFolder + "mission" + to_string(i+1) + ".txt");

			if( mFile.at(i).fail() )
			{
				Logger::logEvent("\n ** Error opening mission trace file. \n");
				exit(3);
			}
		}
	}
}

void Logger::closeOutFile()
{
	outFile.close();

	for(unsigned int i=0; i<mFile.size(); i++){
		mFile.at(i).close();
	}
}

void Logger::logEvent(string evt, bool isTimeStamped, int m)
{
	//Dr. Houston's request include a time stamp on the log entries
	log.lock();
	
	string now = "";

	if(isTimeStamped){
		//for timestamp
		time_facet *facet = new time_facet("%d-%b-%Y %H:%M:%S");
		now = "[" + to_string(second_clock::local_time()) + "] : ";
		cout.imbue(locale(cout.getloc(), facet));
		outFile.imbue(locale(outFile.getloc(), facet));	

		if(m != -1){
			mFile.at(m-1).imbue(locale(mFile.at(m-1).getloc(), facet));
		}
	}

	cout<<now<<evt;
	outFile<<now<<evt;

	if(m != -1){
		mFile.at(m-1)<<now<<evt;
	}

	log.unlock();
}