#ifndef MISSIONPLANNING_H
#define MISSIONPLANNING_H

#include "Node.h"

class MissionPlanning : public Node
{
private:
	string mission_file;
	static bool done;

public:
	MissionPlanning(void);
	MissionPlanning(int, string, string);
	~MissionPlanning(void);
	void operator()();
	static bool isDone();
};

#endif