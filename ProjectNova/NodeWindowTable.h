#ifndef NODEWINDOWTABLE_H
#define NODEWWINDOWTABLE_H

#include <boost/unordered_map.hpp>
#include <boost/thread.hpp>
#include "Node.h"

typedef unsigned int PERIOD;


class Window
{
protected:
	int n1;
	int n2;
	//int open_duration;
	//bool open
	friend bool operator==(const Window& w1, const Window& w2);

public:
	Window();
	Window(const Window&);
	Window(int, int);
	//Window(int, int, int, bool);
	~Window();
	int getN1();
	int getN2();
	//int getOpenDuration();
	//bool isOpen();
};

class NodeWindowTable
{
protected:
	bool loggingEnabled;

private:
	string window_file;
	unordered_map<PERIOD, vector<Window>> dwt;  //dynamic window table
	//vector<Window> currentTable;  //holds the currently open windows
	vector<Window>* currentTable;  //holds the currently open windows
	vector<PERIOD>* periods;

	//Dr. Houston's request: change period length to 20 mins
	int period_length;

public:
	NodeWindowTable();
	NodeWindowTable(string, bool loggingEnabled = false);
	~NodeWindowTable();
	vector<int>* getReachableNodes(int);
	void operator()();
	//void add(Window*);
	//const vector<Window*>* getTable();

	static mutex mtx;
};

#endif