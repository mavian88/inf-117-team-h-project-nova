#include <time.h>
#include <boost/thread.hpp>
#include "CommandCenter.h"
#include "MissionPlanning.h"
#include "Mission.h"
#include "GroundAntenna.h"
#include "Node.h"
#include "MissionRegister.h"
#include "Logger.h"
#include "NodeMap.h"

using namespace boost;


CommandCenter::CommandCenter(void) : Node()
{ }

CommandCenter::CommandCenter(int id, string name) : Node(id, name, "Command Center")
{ }

CommandCenter::~CommandCenter(void)
{ }

void CommandCenter::operator()()
{
	Logger::logEvent(getName() + " has started.. \n");
	
	vector<Mission*>* missions;

	//get all the ground antennas
	vector<Node*>* antennas = new vector<Node*>();
			
	for(auto it = NodeMap::getMap()->begin(); it != NodeMap::getMap()->end(); ++it)
	{
		if(it->second->getType() == "Antenna")
		{
			antennas->push_back(it->second);
		}
	}

	do
	{
		this_thread::sleep(posix_time::milliseconds(1));
		//get missions addressed to me
		missions = MissionRegister::getMissions(getId());

		if( !(*missions).empty() )
		{
			for(Mission* m : *missions)
			{
				//initialize random seed
				srand ((unsigned int) time(NULL));

				//Let's say that transmission takes 1 second to complete
				this_thread::sleep(posix_time::milliseconds(1000));

				Logger::logEvent(getName() + " receives [" + m->getName() + "] from " + to_string(*(m->getSource())) + ". \n", true, m->getId());

				//forward the mission to a ground antenna, choice is random
				//  change the source and destination attributes of this mission
				Node* source = this;
				m->setSource(source);

				int rand_idx = rand() % antennas->size();
				Node* destination = antennas->at(rand_idx);
				m->setDestination(destination);

				Logger::logEvent(getName() + " forwards [" + m->getName() + "] to " + to_string(*destination) + ". \n", true, m->getId());
				MissionRegister::sendToMissionRegister(m);
			}
		}
	}while(!MissionPlanning::isDone() || !(*missions).empty());

	Logger::logEvent(getName() + " is done forwarding missions. \n\n");
}
