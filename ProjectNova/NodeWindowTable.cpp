#include <fstream>
#include <boost/tokenizer.hpp>
#include "NodeWindowTable.h"
#include "NodeMap.h"
#include "Logger.h"

using namespace std;
using namespace boost;

mutex NodeWindowTable::mtx;


Window::Window()
{ }

Window::Window(const Window& w)
{
	this->n1 = w.n1;
	this->n2 = w.n2;
}

Window::Window(int n1, int n2)
{
	this->n1 = n1;
	this->n2 = n2;
}

//Window::Window(int n1, int n2, int open_duration, bool open)
//{
//	this->n1 = n1;
//	this->n2 = n2;
//	this->open_duration = open_duration;
//	this->open = open;
//}


Window::~Window()
{ }

int Window::getN1()
{
	return n1;
}

int Window::getN2()
{
	return n2;
}

//int Window::getOpenDuration()
//{
//	return open_duration;
//}
//
//bool Window::isOpen()
//{
//	return open;
//}

bool operator==(const Window& w1, const Window& w2)
{
	return (w1.n1 == w2.n1 && w1.n2 == w2.n2) ||
				(w1.n2 == w2.n1 && w1.n1 == w2.n2);
}


//NodeWindowTable stuff
NodeWindowTable::NodeWindowTable()
{ }

NodeWindowTable::NodeWindowTable(string window_file, bool loggingEnabled)
{
	this->loggingEnabled = loggingEnabled;

	this->window_file = window_file;
	dwt = unordered_map<PERIOD, vector<Window>>();
	dwt.clear();
	currentTable = new vector<Window>();
	currentTable->clear();

	ifstream inFile;
	inFile.open("windows/" + window_file);

	if( inFile.fail() )
	{
		Logger::logEvent("\n ** Error opening window table file. \n");
		exit(2);
	}

	Logger::logEvent("Preparing Dynamic Window Table.. ");

	//period_length; //the amount of time between table changes
	//first read, get the amount of time between table changes;
	//inFile>>period_length;
	
	string tci;
	getline(inFile, tci);
	period_length = atoi(tci.c_str());

	//the different time periods in the node window table
	periods = new vector<PERIOD>();
	string p_string;
	//get the time periods for the window table
	getline(inFile, p_string);
	tokenizer<> tok(p_string);
	for(tokenizer<>::iterator t=tok.begin(); t!=tok.end();++t){
		PERIOD new_p = atoi((*t).c_str());
		periods->push_back( new_p );

		//make an entry for this period in the dynamic window table
		//  initialize the window vector for this period
		dwt.insert( make_pair(new_p, vector<Window>()) );
		dwt.at(new_p).clear();
	}

	//window attributes
	PERIOD p;
	int n1;
	int n2;

	while( !inFile.eof() )
	{
		inFile>>p>>n1>>n2;
		//Node* n1 = NodeMap::at(n1_id);
		//Node* n2 = NodeMap::at(n2_id); //problem lies in HERE

		//create a new window instance
		Window w = Window(n1, n2);

		//add this new window (if it is unique) to the dynamic window table,
		//  make sure that this new window is inserted in the window vector of the appropriate time period
		if( std::find((dwt.at(p)).begin(), (dwt.at(p)).end(), w) == (dwt.at(p)).end() ){
			(dwt.at(p)).push_back( w );
		}
	}

	*currentTable = dwt.at(1);
	Logger::logEvent("DONE! \n\n", false);
}

NodeWindowTable::~NodeWindowTable()
{ }

vector<int>* NodeWindowTable::getReachableNodes(int from)
{
	vector<int>* reachableNodes = new vector<int>();
	reachableNodes->clear();

	for(Window w : *currentTable)
	{

		if(from == w.getN1())
		{
			reachableNodes->push_back(w.getN2());
		}
		else if(from == w.getN2())
		{
			reachableNodes->push_back(w.getN1());
		}
	}

	return reachableNodes;
}

void NodeWindowTable::operator()()
{
	int i=0;

	while(true)
	{
		this_thread::sleep(posix_time::milliseconds(1));

		//load the window table for the current time period
		mtx.lock();
		*currentTable = dwt.at(periods->at(i));
		mtx.unlock();

		if(loggingEnabled){
			Logger::logEvent("Time Period: " + to_string(periods->at(i)) + " \n");
			for(Window w : *currentTable)
			{
				Logger::logEvent( NodeMap::at(w.getN1())->getName() + " can see " + NodeMap::at(w.getN2())->getName() + ". \n" );
			}
			Logger::logEvent("\n", false);
		}

		this_thread::sleep(posix_time::milliseconds(period_length));
		i = (i+1) % periods->size();
	}
}