#ifndef SIGNAL_H
#define SIGNAL_H

#include "Message.h"

class Signal : public Message
{
private:
	Node* signal_target; //the target antenna that will be the final recipient for the signal
	int signal_duration; //for how long should the collector collect signals from the target
	friend std::ostream& operator<<(std::ostream&, const Signal&);
public:
	Signal(void);
	Signal(int, string, Node*, Node*, Node*, int);
	Signal(const Signal&);
	~Signal(void);
	Node* getTarget(void);
	int getDuration(void);
	bool operator==(const Signal&) const;
};

#endif